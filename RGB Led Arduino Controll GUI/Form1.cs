﻿using System;
using System.Windows.Forms;
using System.IO.Ports;

namespace RGB_Led_Arduino_Controll_GUI
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            serialPort1 = new System.IO.Ports.SerialPort("COM3", 9600);
            serialPort1.Open();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("8");
            serialPort1.WriteLine("n");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("7");
            serialPort1.WriteLine("n");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("9");
            serialPort1.WriteLine("n");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("9");
            serialPort1.WriteLine("f");
            serialPort1.WriteLine("8");
            serialPort1.WriteLine("f");
            serialPort1.WriteLine("7");
            serialPort1.WriteLine("f");
        }
    }
}